import React, { useState } from "react";
import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { Routes, Route, Link, useNavigate } from "react-router-dom";
import "./styles/index.css";
import ProfileForm from "./ProfileForm";
import EducationForm from "./EducationForm";
import Skills from "./Skills";
import Experience from "./Experience";
import Social from "./Social";
import Resume from "../Resume/Index";
import { useSelector } from "react-redux";

function Index() {
  const navigate = useNavigate();
  
  const [activeStep, setActiveStep] = useState(0);
  const [completed, setCompleted] = useState({});
  const profileInformation = useSelector((store) => store.profileData);
  const experienceInfo = useSelector((store) => store.experienceData);
  const educationInfo = useSelector((store) => store.educationData);
  const skillInfo = useSelector((store) => store.skillData);
  const socialInfo = useSelector((store) => store.socialData);
  const [showForm, setShowForm] = useState(0);
  const [showResume, setShowResume] = useState(0);
 
  const completedSteps = () => {    
    return Object.keys(completed).length;
  };
  const totalSteps = () => {
    return steps.length;
  };
  const steps = [
    { label: "Profile Section", path: "/", element: <ProfileForm /> },
    { label: "Experience", path: "/experience", element: <Experience /> },
    {
      label: "Education Section",
      path: "/education",
      element: <EducationForm />,
    },
    { label: "Skills Sector", path: "/skills", element: <Skills /> },

    { label: "Social", path: "/social", element: <Social /> },
  ];

  function getSteps() {
    return [
      "Profile Section",
      "Experience",
      "Education Section",
      "Skills Sector",
      "Social",
    ];
  }

  const allStepsCompleted = () => {
    return completedSteps() === totalSteps();
  };

  const handleReset = () => {
    window.location.reload(false);
    navigate("/profile");
  };
  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };
  const handleNext = () => {    
    const newActiveStep =
      isLastStep() && !allStepsCompleted()
        ? steps.findIndex((step, i) => !(i in completed))
        : activeStep + 1;


    if (newActiveStep < steps.length) {
        setActiveStep(newActiveStep);    
        navigate(steps[newActiveStep].path);
    } else {      
      setShowResume(1);
    }
};

  const handleBack = () => {
    const newActiveStep = activeStep - 1;

    if (newActiveStep >= 0 && newActiveStep < steps.length) {
      setActiveStep(newActiveStep);
      navigate(steps[newActiveStep].path);
    }
  };

  const ValidateProfileDetails = (profileInfo) => {
    if (!profileInformation) {
      alert("Profile information is missing.");
      return false;
    }
    if (
      !profileInformation.overview ||
      profileInformation.overview.trim().length === 0
    ) {
      alert("Please provide an overview.");
      return false;
    }

    let sentence = profileInformation.overview.trim();
    let words = sentence.split(/\s+/);

    if (
      !profileInformation.fname ||
      !profileInformation.lname ||
      !profileInformation.phone ||
      !profileInformation.address
    ) {
      alert("Please fill all the data.");
      return false;
    }

    if (
      profileInformation.fname.length < 1 ||
      profileInformation.lname.length < 1 ||
      profileInformation.address.length < 1
    ) {
      alert("Please fill all the data.");
      return false;
    }

    if (
      profileInformation.phone.length !== 10 &&
      profileInformation.phone.length !== 12
    ) {
      alert("Enter a valid phone number.");
      return false;
    }

    if (words.length < 10) {
      alert("Enter a minimum of 10 words in the overview.");
      return false;
    }

    return true;
  };

  const validateEducationDetails = () => {
    if (!educationInfo) return false;
    const Data = educationInfo;
    for (let i = 0; i < Data.length; i++) {
      const instance = Data[i];
      if (
        !instance.courseName ||
        !instance.college
      ) {
        alert("Please fill all the required fields (Course Name, College)");
        return false;
      }
  
      if (
        instance.courseName.length < 1 ||
        instance.college.length < 1
      ) {
        alert("Incomplete or invalid data");
        return false;
      } 
      
      if (instance.completionYear && instance.completionYear.length === 4) {
        if (!instance.percentage || isNaN(parseFloat(instance.percentage))) {
          alert("Percentage must be a valid number if Completion Year is provided");
          return false;
        }
      }
    }
  
    return true;
  };
  
  

  const validateExperienceDetails = () => {
    if (!experienceInfo) return false;
    let hasEndDate = false; // Flag to track if at least one experience has an end date
    for (let i = 0; i < experienceInfo.length; i++) {
      const instance = experienceInfo[i];
      if (
        !instance.jobtitle ||
        !instance.employer ||
        !instance.startdate
      ) {
        alert("Please fill all the required fields (Job Title, Employer, Start Date)");
        return false;
      }
  
      if (
        instance.jobtitle.length < 1 ||
        instance.employer.length < 1 ||
        instance.startdate.length < 10
      ) {
        alert("Incomplete or invalid data");
        return false;
      }
       
      if (instance.enddate) {
        hasEndDate = true;
      }
    }
  
    if (!hasEndDate) {
      alert("At least one experience must have an end date");
      return false;
    }
  
    return true;
  };
  

  const validateSkills = () => {
    if (skillInfo.length < 1) {
      alert("Please enter your skill");
      return false;
    }
    for (let i = 0; i < skillInfo.length; i++) {
      if (!skillInfo[i] || (skillInfo[i] && skillInfo[i].length < 1)) {
        alert("Please fill all skills");
        return false;
      }
    }
    return true;
  };

  const validateSocialLinks = () => {
    if (socialInfo.length < 1) {
      alert("Please enter your social url");
      return false;
    }
    for (let i = 0; i < socialInfo.length; i++) {
      if (!socialInfo[i] || (socialInfo[i] && socialInfo[i].length < 1)) {
        alert("Please fill all urls");
        return false;
      }
    }
    return true;
  };

  const handleComplete = () => {    
    let flag = true;
    const action = getSteps()[activeStep];
    if (action === "Profile Section") {
      flag = ValidateProfileDetails();      
    } else if (action === "Experience") {
      flag = validateExperienceDetails();      
    } else if (action === "Education Section") {
      flag = validateEducationDetails();      
    } else if (action === "Skills Sector") {      
      flag = validateSkills();
    } else if (action === "Social") {
      flag = validateSocialLinks();      
    }    
    if (flag) {
      const newCompleted = completed;
      newCompleted[activeStep] = true;      
      setCompleted(newCompleted);
      handleNext();
    }
  };

  const handleEdit = () => {
    setShowResume(0);
    setCompleted({});
    setActiveStep(0);
  };
  
  return (
    showForm === 0 ? 
    <Button
      onClick={() => {
        setShowForm(1);
      }}
    >
      Start Building My Resume
    </Button>
    :
    <div className="root">
      <Box sx={{ width: "100%" }}>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map((step, index) => (
            <Step key={index}>
              <StepLabel>
                <Link to={step.path}>{step.label}</Link>
              </StepLabel>
            </Step>
          ))}
        </Stepper>
      </Box>
      <Routes>
        {steps.map((step, index) => (
          <Route key={index} path={step.path} element={step.element} />
        ))}
      </Routes>
      <div>
      {showResume ? (
          <div>
            <Typography className="instructions">
              All steps completed - your resume is ready!!
            </Typography>
            <Button onClick={handleReset}>Reset</Button>
            <Button onClick={handleEdit}>Edit</Button>
            <Resume />
          </div>
        ) : (
          <div>
            <Typography className="instructions">
              {steps[activeStep].component}
            </Typography>
            <div className="navigation-buttons">
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className="button"
              >
                Back
              </Button>
              {completedSteps() < totalSteps() - 1 ?
              <Button
                variant="contained"
                color="secondary"
                onClick={handleNext}
                className="button"
              >
                Next
              </Button>
              : null
              }
              <Button
                variant="contained"
                color="secondary"
                onClick={handleComplete}
              >
                {completedSteps() === totalSteps() - 1
                  ? "Finish"
                  : "Save and Continue"}
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default Index;
