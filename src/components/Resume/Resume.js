import React from "react";
import {  
  Grid,
  Typography,
  Paper,
  Divider,
} from "@mui/material";
import { useSelector } from "react-redux";
import "./resume.css";
function Resume() {
  const profileInformation = useSelector((store) => store.profileData);
  const experienceInfo = useSelector((store) => store.experienceData);
  const educationInfo = useSelector((store) => store.educationData);
  const skillInfo = useSelector((store) => store.skillData);
  const socialInfo = useSelector((store) => store.socialData);

  return (
    <div>
      <Paper className="ParentResumeModel" elevation={1}>
        <div elevation={1} className="ParentResumePaper">
          <Grid container spacing={3}>
            {/* ---------------------------------------------------------------------------------------------------------------------------- */}
            {/* PHOTO , NAME AND ADDRESS */}
            <Grid item xs={3}>
              <div className="profilePhoto">
                {profileInformation ? (
                  <img
                    src={profileInformation.url}
                    alt="Please update url"
                    width="100px"
                    heigh="100px"
                  ></img>
                ) : null}
              </div>
            </Grid>
            <Grid item xs={9}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Typography variant="h4" component="h2">
                    {profileInformation ? profileInformation.fname : null}{" "}
                    {profileInformation ? profileInformation.lname : null}
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant="subtitle1">
                    Address :{" "}
                    {profileInformation ? profileInformation.address : null}
                  </Typography>
                  <Typography variant="subtitle1">
                    Phone Number:{" "}
                    {profileInformation ? profileInformation.phone : null}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            {/* ---------------------------------------------------------------------------------------------------------------------------- */}
            {/* OVERVIEW */}
            <Grid item xs={12}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Typography variant="h5" component="h2">
                    Profile Overview
                  </Typography>
                  <Divider style={{ background: "#abeb34" }} />
                </Grid>
                <Grid item xs={12}>
                  <Typography variant="subtitle1">
                    {profileInformation ? profileInformation.overview : null}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            {/* ---------------------------------------------------------------------------------------------------------------------------- */}
            {/* SKILLS */}
            <Grid item xs={12}>
              <Grid container spacing={3}>
                <Grid item xs={3}>
                  <Grid container spacing={3}>
                    {/* Skills */}
                    <Grid item xs={12}>
                      <div className="ParentSkillSection">
                        <Typography variant="h5" component="h2">
                          Skills
                        </Typography>
                        <Divider style={{ background: "#abeb34" }} />
                        {skillInfo &&
                          skillInfo.length > 0 &&
                          skillInfo.map((item, index) => (
                            <li key={index}>{item}</li>
                          ))}
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={9}>
                  <Grid container spacing={3}>
                    {/* EDUCATION */}
                    <Grid item xs={12}>
                      <div className="header">
                        <Typography variant="h5" component="h2">
                          Education
                        </Typography>
                      </div>
                      <Divider style={{ background: "#abeb34" }} />

                      {educationInfo &&
                        educationInfo.length > 0 &&
                        educationInfo.map((instance, index) => (
                          <div className="content" key={index}>
                            <Typography variant="h6" component="h2">
                              {instance.college ? instance.college : null}
                            </Typography>
                            <Typography variant="body2">
                              {instance.completionYear
                                ? " Graduation Year : " +
                                  instance.completionYear
                                : null}
                            </Typography>
                            <Typography variant="body2">
                              {instance.courseName ? instance.courseName : null}
                            </Typography>
                            <Typography variant="body2">
                              {instance.percentage
                                ? " Percentage : " + instance.percentage + "%"
                                : null}
                            </Typography>
                          </div>
                        ))}
                    </Grid>
                    {/* Education */}
                    <Grid item xs={12}>
                      <div className="header">
                        <Typography variant="h5" component="h2">
                          Education
                        </Typography>
                      </div>
                      <Divider style={{ background: "#abeb34" }} />

                      {educationInfo &&
                        educationInfo.length > 0 &&
                        educationInfo.map((instance, index) => (
                          <div className="content" key={index}>
                            <Typography variant="h6" component="h2">
                              {instance.college ? instance.college : null}
                            </Typography>
                            <Typography variant="body2">
                              {instance.completionYear
                                ? " Graduation Year : " +
                                  instance.completionYear
                                : " Graduation Year : Doing"}
                            </Typography>
                            <Typography variant="body2">
                              {instance.courseName ? instance.courseName : null}
                            </Typography>
                            <Typography variant="body2">
                              {instance.percentage
                                ? " Percentage : " + instance.percentage + "%"
                                : null}
                            </Typography>
                          </div>
                        ))}
                    </Grid>

                    <Grid item xs={12}>
                      {/* EXPERIENCE */}
                      <div className="header">
                        <Typography variant="h5" component="h2">
                          Experience
                        </Typography>
                      </div>
                      <Divider style={{ background: "#abeb34" }} />

                      {experienceInfo &&
                        experienceInfo.length > 0 &&
                        experienceInfo.map((instance, index) => (
                          <div className="content" key={index}>
                            <Typography variant="h6" component="h2">
                              {instance.jobtitle ? instance.jobtitle : null}
                            </Typography>
                            <Typography variant="body2" gutterBottom>
                              {instance.employer ? instance.employer : null}
                            </Typography>
                            <Typography variant="body2" gutterBottom>
                              {instance.startdate
                                ? "Duration: " +
                                  instance.startdate
                                    .toISOString()
                                    .split("T")[0] +
                                  (instance.enddate
                                    ? " - " +
                                      instance.enddate
                                        .toISOString()
                                        .split("T")[0]
                                    : " - Present")
                                : null}
                            </Typography>
                          </div>
                        ))}
                    </Grid>
                    {/* SOCIAL */}
                    <Grid item xs={12}>
                      <div className="ParentSkillSection">
                        <Typography variant="h5" component="h2">
                          Social Links
                        </Typography>
                        <Divider style={{ background: "#abeb34" }} />
                        {socialInfo &&
                          socialInfo.length > 0 &&
                          socialInfo.map((item, index) => (
                            <li key={index}>
                              <a href={item} target="_blank" rel="noreferrer">
                                {item}
                              </a>
                            </li>
                          ))}
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </Paper>
    </div>
  );
}

export default Resume;
