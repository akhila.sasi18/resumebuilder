# Resume Builder


#### Resume Builder 

A beautifully crafted personal resume developed using React. The design exudes an atmospheric aesthetic, while the code is built upon the foundations of React, Redux, JavaScript, and CSS. The project showcases excellent scalability and maintainability, making it a standout example of modern web development practices.


## Project Screen Shot(s)


![Resume-Builder1](src/assets/Resume-Builder1.png "Resume-Builder1"){width=75%}
![Resume-Builder-Social](src/assets/Resume-Builder-Social.png "Resume-Builder-Social"){width=75%}
![Resume-Builder-Resume](src/assets/Resume-Builder-Resume.png "Resume-Builder-Resume"){width=75%}

## Installation and Setup Instructions

 

Clone down this repository. You will need `node` and `npm` installed globally on your machine.  

Installation:

`npm install`   

To Start Server:

`npm start`  

To Visit App:

`localhost:3000/`  

## About

I have recently completed a one-week project to develop a Resume Builder Application using React. This project was undertaken as part of my ReactJS course, with the primary goals being to apply the technologies learned during the course and to become familiar with the documentation for new features.

Key features of the application include:

- Resume Creation: Users can easily create and customize their resumes by adding personal information, education details, work experience, skills, and more.

- Preview Functionality: Users have the option to preview their resumes before finalizing them, allowing them to make any necessary adjustments.

**Technologies used in this project include:**

- React: The core framework for building the user interface and managing state.

- React Router: Version 6.21.3 was integrated for managing navigation within the application.

- Redux: Used for state management, providing a centralized store for application data.



Initially, I utilized the create-react-app boilerplate to streamline the setup process.

Overall, this project allowed me to explore a wide range of technologies and dive into various technological challenges, contributing to my growth as a React developer. In future iterations, I aim to enhance the application further and continue expanding my skills in React development.
